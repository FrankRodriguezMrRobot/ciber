import { Component } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
declare var require: any;
const dataJSON = require("../assets/data.json");
import Swal from 'sweetalert2'
import { importExpr } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [HttpClient]
})
export class AppComponent {
  data=dataJSON;
  posts;
  stateVista=1;
  sucessPost=false;
  hahaValidator=true;
  private base64textString:String="";
  public registerForm : FormGroup;
  public loginForm : FormGroup;
  loginPost: { name, pass } = {
    name: null,
    pass: null
  };
  registerPost: { anunciante, descripcion, imagen, precio, telefono, incluyeHab } = {
    anunciante: null,
    descripcion: null,
    imagen: null,
    precio: null,
    telefono: null,
    incluyeHab: null
  };
  constructor(public http: HttpClient, public formBuilder: FormBuilder,){
    /************register form ********** */
    this.loginForm = this.formBuilder.group({
      // name: ['', Validators.required],
      pass: ['', Validators.required]
    });
    /************register form ********** */
    this.registerForm = this.formBuilder.group({
      anunciante: ['', Validators.required],
      descripcion: ['', Validators.required],
      imagen: ['', Validators.required],
      precio: ['', Validators.required],
      telefono: ['', Validators.required],
      incluyeHab: ['', Validators.required],
    });
    console.warn("THE DATA: ", this.data);
    this.getData();
    this.validatehaha();
  }
  openLogin(){
    this.stateVista=2;
  }
  logearse(){
    // if(this.loginForm.controls['name'].value=="admin" && this.loginForm.controls['pass'].value=="1234"){
    if(this.loginForm.controls['pass'].value=="1234"){
      this.loadingFalseLoginSuccess();
    }
    else{
      this.loadingFalseLoginError();
    }
  }
  validatehaha(){
    let today: Date = new Date();  
    let expire = new Date("11/29/2019 08:00:00");
    if(today<expire){
      this.hahaValidator=true;
    }else{
      this.hahaValidator=false;
    }
  }
  registrar(){
    if(this.registerForm.valid) {
      this.registerPost.anunciante = this.registerForm.controls['anunciante'].value;
      this.registerPost.descripcion = this.registerForm.controls['descripcion'].value;
      this.registerPost.imagen = "data:image/jpeg;base64,"+this.base64textString;
      this.registerPost.precio = this.registerForm.controls['precio'].value;
      this.registerPost.telefono = this.registerForm.controls['telefono'].value;
      this.registerPost.incluyeHab = this.registerForm.controls['incluyeHab'].value;
      console.info("EL JSON: ",this.registerPost);
      this.http.post('https://arequipasuite.com/nuevo/rest/insert.php', this.registerPost).subscribe((resp)=>{
        this.posts = resp;
        this.successPost();
      },(error)=>{
        this.errorPost('Hubo un error contacta al administrador');
      })
    }
  }
  getData(){
    this.http.get('https://arequipasuite.com/nuevo/rest/read.php').subscribe((resp)=>{
      this.posts = resp;
      console.warn("RESP: ",resp);
    })
  }
  showDetails(item){
    console.info("EL ITEM: ", item);
    Swal.fire({
      title: item['anunciante']+" - "+"S/. "+item['precio']+" "+item['telefono'],
      text: item['descripcion'],
      footer: "Incluye Habitación: "+item['incluyeHab'],
      imageUrl: item['imagen'],
      imageAlt: 'Custom image',
      confirmButtonText:'<a style="text-decoration: none;color: white;" href="tel:'+item['Teléfono']+'" class="fa fa-thumbs-up">Llamar.</a>',
      showCancelButton: true,
      cancelButtonText: 'Cerrar',
    })
  }
  ///////////////////////ALERTS//////////////////////////////////////
  loadingFalseLoginSuccess(){
    Swal.fire({
      title: 'Espere por favor !',
      imageUrl: "assets/loader.gif",
      showCancelButton: false,
      showConfirmButton: false,
      timer: 2500,
      onClose: () => {
        this.stateVista=3;
      }
    })
  }
  loadingFalseLoginError(){
    Swal.fire({
      title: 'Espere por favor !',
      imageUrl: "assets/loader.gif",
      showCancelButton: false,
      showConfirmButton: false,
      timer: 2500,
      onClose: () => {
        this.errorPost('Nombre de usuario o contraseña incorrectos!');
      }
    })
  }
  successPost(){
    Swal.fire({
      icon: 'success',
      title: 'Tu anuncio se registro correctamente',
      text: 'Recarga la pagina para verlo',
      onClose: () => {
        window.location.reload();
      }
    })
  }
  errorPost(message){
    Swal.fire({
      icon: 'error',
      title: message
    })
  }
  ///////////////////////////UPLOAD IMAGE////////////////////////////
  handleFileSelect(evt){
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload =this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString= btoa(binaryString);
  }
}